import React,{useEffect, useState} from 'react';
import Child from './child'
import styles from './Button.module.css'

function Parent(props) {
    const [city,setCity] = useState('')
    const [data,setData] = useState('')
   

    const onChange = (e) => {
        setCity(e.target.value)
    }
    const onSubmit = async e => {
        e.preventDefault()
            const res = await fetch(`https://api.weatherapi.com/v1/current.json?key=2626bb280087465496b34227211901&q=${city}`)
            const res_data = await res.json()
            setData(res_data)
            
    }
    
    
    return(
        <div >
            <form onSubmit ={onSubmit} className="flex    pb-20">
                
                <input type="text" value={city} onChange={onChange} className="rounded-l-lg p-4 border-t border-l border-b border-gray-200 bg-white text-gray-800 bg-white" placeholder="Enter Search City"/>
        
                < button type='submit' className="px-10  rounded-r-lg bg-yellow-400 text-gray-800 font-bold border-yellow-500 border-t border-b border-r">Get Weather</button>
            </form>
            
            {/* current information */}
            {data.current && <Child data={data} /> }
            {console.log(data)}
            
            
            
          
            
        </div>
    )
}
export default Parent
