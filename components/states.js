import React from 'react';
class Example extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count:0
        }
    }

    render() {
        return(
            <div>
                <p>{this.state.count}</p>
                <p>You clicked {this.state.count} times</p>
                <button onClick={() => this.setState({count: this.state.count +2})}>
                    Click the states
                </button>
            </div>
        )
    }
}
export default Example