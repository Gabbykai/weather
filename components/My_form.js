import React,{useState} from 'react';

function Form() {
  const [city,setCity] = useState('')
  const [data,setdata] = useState('')

  const onChange = (e) => {
    setCity( e.target.value)
  }
}

const onSubmit = async event => {
    e.preventDefault();

   //fetching now
    const res = await fetch(`https://api.weatherapi.com/v1/current.json?key=2626bb280087465496b34227211901&q=${city}`)
    const res_data = await res.json()
    setdata(res_data)
    
     
}


  return(
    <div>
      <form onSubmit={onSubmit}>
        <label htmlFor="city">Enter a city:
        <input  type="text" name ="city"onChange={onChange} />
       </label>

       <button type="submit" value="Submit" />
      </form>
      <div class="card border-8 border-pink-800 rounded bg-white shadow max-w-md mx-40 mt-40">
                {/* main card content */}
               <div class="pl-28">
                 <div >
                    <h2 >{props.data.location.name},{props.data.location.country}</h2>
                 </div>
                   {/*temperature  */}
                 <div>
                    <h3>{props.data.current.temp_c}&deg;C</h3>
                 </div>
                 {/* weather icon of the current weather */}
                 <div>
                  <img   src={props.data.current.condition.icon} />
                 </div>

                 <div class="h3"> {props.data.current.condition.text}</div>
                 </div>
                
       </div>
       </div>

    
  )

export default Form