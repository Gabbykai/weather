
import { data } from 'autoprefixer'
import React,{Component} from 'react'
import Parent from './Parent'
import Get_sunny from './get_sunny'
import WiSnow, { WiFahrenheit, WiHumidity, WiWindy } from 'react-icons/wi'
import { icons } from 'react-icons/lib'
import { loadGetInitialProps } from 'next/dist/next-server/lib/utils'
import { WiUmbrella,WiBarometer,WiThermometer } from 'react-icons/wi'

function Child(props) {
      
    
    return(
        <div className = "min-h-screen  min-w-full  border  ">
            <div className='flex flex-row pb-40 '>
               <div className=''> <Get_sunny code={props.data.current.condition.code} /></div>
                <div className='text-2xl'>{props.data.current.temp_c}<sup>o</sup></div>

                <div className='flex flex-col  absolute right-1/3 font-mono italic text-blue-600'>
                    {props.data.location.name},{props.data.location.region}
                    <div>lat:{props.data.location.lat},lon:{props.data.location.lon}</div>
                    <div>feels_like:{props.data.current.feelslike_c}<sup>o</sup></div>
                    <div>{props.data.current.condition.text}</div>
                </div>
            </div>

            <table className=' relative top-2/3 pt-10' >
                <thead>
                    <tr >
                       <th> <WiUmbrella  className='text-9xl text-current'/></th>
                       <th><WiBarometer className='text-9xl '/></th>
                       <th><WiThermometer className='text-9xl '/></th>
                       <th><WiWindy className='text-9xl '/></th>
                       <th><WiHumidity className='text-9xl'/></th>
                    </tr>
                    
                   
                </thead>
                <tbody>
                    <tr className='text-xl text-center text-purple-900 font-sans'>
                        <td >{props.data.current.precip_mm} mm</td>
                        <td>{props.data.current.pressure_mb}mb</td>
                        <td>{props.data.current.temp_c}<sup>o</sup>C</td>
                        <td>{props.data.current.wind_kph}Kmh</td>
                        <td className='' >{props.data.current.humidity}%</td>
                    </tr>
                </tbody>
            </table>
              

               
            


        </div>
            
    )
}
export default Child