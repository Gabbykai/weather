import React , {useState} from 'react';

function Appl() {

    const [name,setname] = useState('Gabriel')

    const [count,setCount] = useState(0);
    return(
        <div>
            <h1>Programmer: {name}</h1>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count+1)}>
                Click me
            </button>

        </div>
    );
};
export default Appl

