import Head from 'next/head'
import styles from '../styles/Home.module.css'

import Parent from '../components/Parent'
import Link from 'next/link'
import {WiAlien} from "react-icons/wi"







export default function Home(){
  

  return (
    <div className="h-screen overflow-visible flex justify-center" >
     <div className="flex flex-col">
       <div className=' static flex items-stretch'>
        <img src='/weather_system.png '  />
        <h1 className="text-6xl font-serif font-bold align-middle relative  inset-y-10" >Weather Today</h1>
       </div>
       <Parent />
      </div>
    </div>
   
  )
}
console.log('/weather_system.png')

